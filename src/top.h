#ifndef TOP_H_
#define TOP_H_

#include <stdlib.h>
#include <unistd.h>
#include "kuso/kuso.h"

enum {
  OUTPUT_TYPE_EXE = 0,
  OUTPUT_TYPE_OBJECT,
  OUTPUT_TYPE_ASM,
  OUTPUT_TYPE_ASM_WITH_C,
};

typedef struct {
  kuso_pvec_t *include_dirs; // const char *
  kuso_pvec_t *defines; // const char *
} CPPState;

extern const char *g_app;
extern const char *g_in_file;
extern const char *g_out_file;
extern const char *g_lsic86;
extern int g_verbosity;
extern int g_dryrun;
extern int g_out_type;

extern CPPState g_cpp;

#endif
