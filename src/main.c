#include "top.h"

const char *g_app;
const char *g_in_file = NULL;
const char *g_out_file = NULL;

typedef struct {
  kuso_pvec_t *segments;
} AsmState;

AsmState g_asm;

void writestr(const char *str, FILE *fp) {
  fwrite(str, strlen(str), 1, fp);
}

int strstart(const char *str, const char *cmp) {
  if (strncmp(str, cmp, strlen(cmp)) == 0) {
    return 1;
  } else {
    return 0;
  }
}

int pragma_pushseg(const char *line, FILE *ofp) {
  kuso_pvec_append(g_asm.segments, mem_strdup(line));
  writestr(line, ofp);
  return 1;
}
int pragma_popseg(const char *line, FILE *ofp) {
  mem_free(kuso_pvec_pop(g_asm.segments));
  if (kuso_pvec_size(g_asm.segments) == 0) {
    fprintf(stderr, "Error: Segment pop underflow\n");
    return 0;
  }
  const char *oseg;
  oseg = kuso_pvec_pop_type(g_asm.segments, const char *);
  kuso_pvec_append(g_asm.segments, oseg);
  writestr(oseg, ofp);
  return 1;
}
int pragma_asm(const char *line, FILE *ofp) {
  writestr(line, ofp);
  return 1;
}
int process_pragma(char *line, FILE *ofp) {
  fprintf(stderr, "tpragma: '%s'\n", line);
  if (strchr(line, '>') == NULL) {
    fprintf(stderr, "Warning: tpragmas should end with '>'\n");
  } else {
    *strchr(line, '>') = '\0';
  }
  if (strstart(line, "push_segment<")) {
    return pragma_pushseg(line + strlen("push_segment<"), ofp);
  } else if (strstart(line, "pop_segment<")) {
    return pragma_popseg(line + strlen("pop_segment<"), ofp);
  } else if (strstart(line, "asm<")) {
    return pragma_asm(line + strlen("asm<"), ofp);
  } else {
    fprintf(stderr, "Warning: unknown pragma '%s'\n", line);
    return 1;
  }
}

// Gather any data we may want
void introspect_line__segment(const char *line) {
  char *ptr;
  ptr = strstr(line, "CSEG");
  if (ptr == NULL) {
    ptr = strstr(line, "DSEG");
  }
  if (ptr == NULL) {
    ptr = strstr(line, "ESEG");
  }
  if (ptr != NULL) {
    while(kuso_pvec_size(g_asm.segments)) {
      mem_free(kuso_pvec_pop(g_asm.segments));
    }
    kuso_pvec_append(g_asm.segments, mem_strdup(line));
  }
}
void introspect_line(const char *line) {
  introspect_line__segment(line);
}

void output_header(FILE *ofp) {
  // Set r86 to 80186 mode
  writestr("\t.186\n", ofp);
}

int process_line(char *line, FILE *ofp) {
  if (strstart(line, ";//#tpragma ")) {
    return process_pragma(line + strlen(";//#tpragma "), ofp);
  } else if (strncmp(line, "\tEND", 4) == 0) {
    fprintf(stderr, "END\n");
    return 0;
  } else {
    if (strlen(line) == 0) {
      return 1;
    }
    introspect_line(line);
    writestr(line, ofp);
    return 1;
  }
}

void usage(void) {
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "\t%s out.a86 in.a86\n", g_app);
}

int main(int argc, char *argv[]) {
  g_app = argv[0];
  if (argc < 3) {
    fprintf(stderr, "Incorrect arguments.\n");
    goto _fail;
  }
  g_out_file = argv[1];
  g_in_file = argv[2];

  g_asm.segments = kuso_pvec_new(0);

  char *str;
  size_t len;
  FILE *fp = fopen(g_in_file, "r");
  fseek(fp, 0, SEEK_END);
  len = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  str = mem_alloc(len);
  fread(str, len, 1, fp);
  fclose(fp);
  fp = fopen(g_out_file, "w");

  output_header(fp);

  char *ptr = str;
  while (ptr[0] != '\0') {
    char c;
    int ret;
    str = ptr;
    ptr = kuso_nextline(str);
    c = ptr[0];
    ptr[0] = '\0';
    ret = process_line(str, fp);
    if (ret < 0) {
      goto _fail;
    }
    if (ret == 0) {
      break;
    }
    ptr[0] = c;
  }

  fclose(fp);
  return EXIT_SUCCESS;
_fail:
  usage();
  return EXIT_FAILURE;
}
