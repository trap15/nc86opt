#ifndef LIBKUSO_YAML_H
#define LIBKUSO_YAML_H

enum {
  YAML_NULL = 0,
  YAML_BOOL = 1,
  YAML_STR = 2,
  YAML_INT = 3,
  YAML_FLOAT = 4,
  YAML_SEQ = 5,
  YAML_MAP = 6,
};

typedef struct yaml_t yaml_t;

struct yaml_t {
  yaml_t* next;
  yaml_t* prev;
  yaml_t* child;

  int type;
  char* name;

  char* val_s;
  int val_i;
  double val_f;
};

yaml_t* kuso_yaml_parse(const char* string);
void    kuso_yaml_delete(yaml_t* yml);

void    kuso_yaml_print(yaml_t* yml);
const char* kuso_yaml_problem_ptr(void);
const char* kuso_yaml_problem(void);

int     kuso_yaml_count(yaml_t* yml);

yaml_t* kuso_yaml_get_map_item(yaml_t* yml, const char *str);
int     kuso_yaml_get_map_str(yaml_t* yml, const char* str, char** val);
int     kuso_yaml_get_map_int(yaml_t* yml, const char* str, int* val);
int     kuso_yaml_get_map_bool(yaml_t* yml, const char* str, int* val);
int     kuso_yaml_get_map_flt(yaml_t* yml, const char* str, double* val);
int     kuso_yaml_get_map_obj(yaml_t* yml, const char* str, yaml_t** val);

yaml_t* kuso_yaml_get_seq_item(yaml_t* yml, int idx);
int     kuso_yaml_get_seq_str(yaml_t* yml, int idx, char** val);
int     kuso_yaml_get_seq_int(yaml_t* yml, int idx, int* val);
int     kuso_yaml_get_seq_bool(yaml_t* yml, int idx, int* val);
int     kuso_yaml_get_seq_flt(yaml_t* yml, int idx, double* val);
int     kuso_yaml_get_seq_obj(yaml_t* yml, int idx, yaml_t** val);

#endif
