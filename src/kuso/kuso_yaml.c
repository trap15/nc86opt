#include "kuso.h"

#if !KUSO_NO_YAML
#include <yaml.h>

#define YAML_TYPE_IS_STR(x) (((x) == YAML_STR) || \
                             ((x) == YAML_INT) || \
                             ((x) == YAML_BOOL) || \
                             ((x) == YAML_FLOAT))
#define YAML_TYPE_IS_OBJ(x) (((x) == YAML_MAP) || \
                             ((x) == YAML_SEQ))

#define YAML_TYPE_IS_INT(x)  ((x) == YAML_INT)
#define YAML_TYPE_IS_BOOL(x) ((x) == YAML_BOOL)
#define YAML_TYPE_IS_FLT(x)  ((x) == YAML_FLOAT)
#define YAML_TYPE_IS_MAP(x)  ((x) == YAML_MAP)
#define YAML_TYPE_IS_SEQ(x)  ((x) == YAML_SEQ)


typedef struct yaml_psr_t yaml_psr_t;
struct yaml_psr_t {
  yaml_parser_t psr;

  const char* str;
  size_t len;
};

static const char* _yaml_errptr = NULL;
static const char* _yaml_errstr = NULL;

const char* kuso_yaml_problem_ptr(void)
{
  return _yaml_errptr;
}
const char* kuso_yaml_problem(void)
{
  return _yaml_errstr;
}

static yaml_t* kuso_yaml_new_entry(yaml_psr_t* psr)
{
  yaml_t* yml;
  (void)psr;

  yml = mem_alloc_type(yaml_t);
  yml->next = yml->prev = yml->child = NULL;

  yml->type = YAML_NULL;
  yml->name = NULL;

  yml->val_s = NULL;
  yml->val_i = 0;
  yml->val_f = 0.0;

  return yml;
}

static void kuso_yaml_set_value(yaml_t* yml, yaml_token_t* tkn)
{
  char* tknstr = (char*)tkn->data.scalar.value;
  yml->val_s = mem_strdup(tknstr);

  yml->type = YAML_STR;

  if(strcasecmp(tknstr, "false") == 0) {
    yml->type = YAML_BOOL;
    yml->val_i = 0;
  }else if(strcasecmp(tknstr, "true") == 0) {
    yml->type = YAML_BOOL;
    yml->val_i = 1;
  }else if((tknstr[0] == '-') || ((tknstr[0] >= '0') && (tknstr[0] <= '9'))) {
    char* ostr;
    xtoi_full(tknstr, -1, &ostr);
    if(ostr != tknstr) {
      if(ostr[0] == '.') {
        sscanf(tknstr, "%lf", &yml->val_f);
        yml->type = YAML_FLOAT;
      }else{
        yml->val_i = xtoi(tknstr);
        yml->type = YAML_INT;
      }
    }
  }
}

static yaml_t* kuso_yaml_apply_next(yaml_t* syml, yaml_t* yml, yaml_t* par)
{
  (void)par;
  if(yml != NULL) {
    syml->prev = yml->prev;
    if(yml->prev != NULL)
      yml->prev->next = syml;

    syml->next = yml;
    yml->prev = syml;
  }
  return syml;
}

enum {
  YAML_MODE_TOP,
  YAML_MODE_KEY,
  YAML_MODE_VALUE,
};

static yaml_t* kuso_yaml_parse_tree(yaml_psr_t* psr, yaml_t* par)
{
  yaml_token_t tkn;
  yaml_t* yml = NULL;
  yaml_t* syml = NULL;
  int type;
  int done;
  int mode = YAML_MODE_TOP;

  done = 0;
  while(!done) {
    if(!yaml_parser_scan(&psr->psr, &tkn)) {
      _yaml_errptr = psr->str + psr->psr.problem_mark.index;
      _yaml_errstr = psr->psr.problem;
      return NULL;
    }
    type = tkn.type;
    switch(type) {
      case YAML_STREAM_START_TOKEN:
        break;
      case YAML_STREAM_END_TOKEN:
        done = 1;
        break;

      case YAML_KEY_TOKEN:
        syml = kuso_yaml_new_entry(psr);
        mode = YAML_MODE_KEY;
        break;
      case YAML_VALUE_TOKEN:
        mode = YAML_MODE_VALUE;
        break;

      case YAML_BLOCK_SEQUENCE_START_TOKEN:
        if(syml == NULL)
          syml = kuso_yaml_new_entry(psr);

        syml->type = YAML_SEQ;
        if((syml->child = kuso_yaml_parse_tree(psr, syml)) == NULL)
          return NULL;
        yml = kuso_yaml_apply_next(syml, yml, par);
        break;
      case YAML_BLOCK_ENTRY_TOKEN:
        syml = kuso_yaml_new_entry(psr);
        mode = YAML_MODE_VALUE;
        break;
      case YAML_BLOCK_END_TOKEN:
        done = 1;
        break;

      case YAML_BLOCK_MAPPING_START_TOKEN:
        if(syml == NULL)
          syml = kuso_yaml_new_entry(psr);

        syml->type = YAML_MAP;
        if((syml->child = kuso_yaml_parse_tree(psr, syml)) == NULL)
          return NULL;
        yml = kuso_yaml_apply_next(syml, yml, par);
        break;
      case YAML_SCALAR_TOKEN:
        switch(mode) {
          case YAML_MODE_KEY:
            syml->name = mem_strdup((char*)tkn.data.scalar.value);
            break;
          case YAML_MODE_VALUE:
            kuso_yaml_set_value(syml, &tkn);
            yml = kuso_yaml_apply_next(syml, yml, par);
            break;
        }
        mode = YAML_MODE_TOP;
        break;
      default:
        break;
    }
    yaml_token_delete(&tkn);
  }

  return yml;
}

yaml_t* kuso_yaml_parse(const char* string)
{
  yaml_t* yml;
  yaml_psr_t* psr;
  psr = mem_alloc_type(yaml_psr_t);

  psr->str = string;
  psr->len = strlen(psr->str);

  _yaml_errptr = NULL;
  _yaml_errstr = NULL;

  if(!yaml_parser_initialize(&psr->psr)) {
    mem_free(psr);
    printf("ERROR: Initialize YAML parser\n");
    return NULL;
  }

  yaml_parser_set_input_string(&psr->psr, (const unsigned char*)psr->str, psr->len);

  yml = kuso_yaml_parse_tree(psr, NULL);

  yaml_parser_delete(&psr->psr);
  mem_free(psr);

  return yml;
}

static void _print_indent(int indent)
{
  printf("%*s", indent*2, "");
}
static void _print_seq(int seq)
{
  if(seq) printf("- ");
}
static void _print_name(yaml_t* yml)
{
  if(yml->name != NULL)
    printf("%s: ", yml->name);
}
static void kuso_yaml_print_core(yaml_t* yml, int indent, int seq)
{
  switch(yml->type) {
    case YAML_STR:
      _print_indent(indent);
      _print_seq(seq);
      _print_name(yml);
      printf("%s\n", yml->val_s);
      break;
    case YAML_INT:
      _print_indent(indent);
      _print_seq(seq);
      _print_name(yml);
      printf("%d\n", yml->val_i);
      break;
    case YAML_FLOAT:
      _print_indent(indent);
      _print_seq(seq);
      _print_name(yml);
      printf("%f\n", yml->val_f);
      break;
    case YAML_MAP:
      _print_indent(indent);
      _print_seq(seq);
      _print_name(yml);
      printf("\n");
      kuso_yaml_print_core(yml->child, indent+1, 0);
      break;
    case YAML_SEQ:
      _print_indent(indent);
      _print_seq(seq);
      _print_name(yml);
      printf("\n");
      kuso_yaml_print_core(yml->child, indent+1, 1);
      break;
    case YAML_NULL:
      printf("NULL\n");
      break;
  }
  if(yml->next)
    kuso_yaml_print_core(yml->next, indent, seq);
}
void kuso_yaml_print(yaml_t* yml)
{
  kuso_yaml_print_core(yml, 0, 0);
}

static void kuso_yaml_delete_core(yaml_t* yml)
{
  yaml_t* n = yml->next;

  if(yml->name != NULL) mem_free(yml->name);
  if(yml->val_s != NULL) mem_free(yml->val_s);

  switch(yml->type) {
    case YAML_STR:
      break;
    case YAML_INT:
      break;
    case YAML_FLOAT:
      break;
    case YAML_MAP:
      kuso_yaml_delete_core(yml->child);
      break;
    case YAML_SEQ:
      kuso_yaml_delete_core(yml->child);
      break;
    case YAML_NULL:
      break;
  }
  mem_free(yml);

  if(n) kuso_yaml_delete_core(n);
}
void kuso_yaml_delete(yaml_t* yml)
{
  kuso_yaml_delete_core(yml);
}

int kuso_yaml_count(yaml_t* yml)
{
  int cnt = 0;
  yaml_t* y;
  y = yml->child;

  while(y != NULL) {
    cnt++;
    y = y->next;
  }
  return cnt;
}

yaml_t* kuso_yaml_get_map_item(yaml_t* yml, const char *str)
{
  yaml_t* y;

  if(!YAML_TYPE_IS_MAP(yml->type))
    return NULL;

  y = yml->child;
  while(y != NULL) {
    if(y->name == NULL)
      continue;
    if(strcmp(y->name, str) == 0)
      return y;
    y = y->next;
  }
  return NULL;
}

int kuso_yaml_get_map_str(yaml_t* yml, const char* str, char** val)
{
  yaml_t* y;
  y = kuso_yaml_get_map_item(yml, str);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_STR(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_s;
  return 1;
}
int kuso_yaml_get_map_int(yaml_t* yml, const char* str, int* val)
{
  yaml_t* y;
  y = kuso_yaml_get_map_item(yml, str);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_INT(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_i;
  return 1;
}
int kuso_yaml_get_map_bool(yaml_t* yml, const char* str, int* val)
{
  yaml_t* y;
  y = kuso_yaml_get_map_item(yml, str);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_BOOL(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_i;
  return 1;
}
int kuso_yaml_get_map_flt(yaml_t* yml, const char* str, double* val)
{
  yaml_t* y;
  y = kuso_yaml_get_map_item(yml, str);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_FLT(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_f;
  return 1;
}
int kuso_yaml_get_map_obj(yaml_t* yml, const char* str, yaml_t** val)
{
  yaml_t* y;
  y = kuso_yaml_get_map_item(yml, str);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_OBJ(y->type))
    return 0;

  if(val != NULL)
    *val = y;
  return 1;
}

yaml_t* kuso_yaml_get_seq_item(yaml_t* yml, int idx)
{
  int cnt = 0;
  yaml_t* y;

  if(!YAML_TYPE_IS_OBJ(yml->type))
    return NULL;

  y = yml->child;
  while(y != NULL) {
    if(cnt == idx)
      return y;

    cnt++;
    y = y->next;
  }
  return NULL;
}
int kuso_yaml_get_seq_str(yaml_t* yml, int idx, char** val)
{
  yaml_t* y;
  y = kuso_yaml_get_seq_item(yml, idx);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_STR(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_s;
  return 1;
}
int kuso_yaml_get_seq_int(yaml_t* yml, int idx, int* val)
{
  yaml_t* y;
  y = kuso_yaml_get_seq_item(yml, idx);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_INT(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_i;
  return 1;
}
int kuso_yaml_get_seq_bool(yaml_t* yml, int idx, int* val)
{
  yaml_t* y;
  y = kuso_yaml_get_seq_item(yml, idx);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_BOOL(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_i;
  return 1;
}
int kuso_yaml_get_seq_flt(yaml_t* yml, int idx, double* val)
{
  yaml_t* y;
  y = kuso_yaml_get_seq_item(yml, idx);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_FLT(y->type))
    return 0;

  if(val != NULL)
    *val = y->val_f;
  return 1;
}
int kuso_yaml_get_seq_obj(yaml_t* yml, int idx, yaml_t** val)
{
  yaml_t* y;
  y = kuso_yaml_get_seq_item(yml, idx);
  if(y == NULL)
    return 0;
  if(!YAML_TYPE_IS_OBJ(y->type))
    return 0;

  if(val != NULL)
    *val = y;
  return 1;
}

#endif
