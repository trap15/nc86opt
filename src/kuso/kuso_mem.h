/******************************************************************************
*
* libkuso
* Memory layer header
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*******************************************************************************
*
* Data of an allocation is not guaranteed to be any set value unless you use
* the 'zalloc' functions, which will initialize the data to 0.
*
* If an allocation fails, NULL will be returned, and your assigned callback
* will be run. Depending on your application setup, you may not need to check
* for NULL. A setup that doesn't need to check for NULL will have the callback
* set up and that callback will quit the application.
*
******************************************************************************/

#ifndef LIBKUSO_MEM_H_
#define LIBKUSO_MEM_H_

/* When an allocation fails, a callback of this type is run.
 * If not doing a realloc, old is NULL.
 * If not doing a "count" alloc, num = 0.
 * If doing a free, old is the pointer, and num and size are 0.
 */
typedef void (*mem_fail_cb)(void* old, size_t num, size_t size);

void mem_init(void);
void mem_finish(void);
void mem_set_callback(mem_fail_cb cb);

void* mem_alloc(size_t size);
void* mem_alloc_cnt(size_t num, size_t size);
void* mem_zalloc(size_t size);
void* mem_zalloc_cnt(size_t num, size_t size);
void* mem_realloc(void* ptr, size_t size);
void* mem_realloc_cnt(void* ptr, size_t num, size_t size);
char* mem_strdup(const char* str);
void mem_free(void* ptr);


#define mem_alloc_type(t)           ((t*)mem_alloc(sizeof(t)))
#define mem_alloc_cnt_type(c,t)     ((t*)mem_alloc_cnt(c, sizeof(t)))
#define mem_zalloc_type(t)          ((t*)mem_zalloc(sizeof(t)))
#define mem_zalloc_cnt_type(c,t)    ((t*)mem_zalloc_cnt(c, sizeof(t)))
#define mem_realloc_type(p,t)       ((t*)mem_realloc(p, sizeof(t)))
#define mem_realloc_cnt_type(p,c,t) ((t*)mem_realloc_cnt(p, c, sizeof(t)))

#if !KUSO_NO_MEM_OVERRIDE
# include "kuso_mem_disable.h"
#endif

#if KUSO_MEM_TRACKING
# include "kuso_mem_track.h"
#endif

#endif
