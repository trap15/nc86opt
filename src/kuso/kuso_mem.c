/******************************************************************************
*
* libkuso
* Memory layer code
*
* Copyright (C) 2013, Alex 'trap15' Marshall <trap15@raidenii.net>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#define KUSO_NO_MEM_OVERRIDE 1
#include "kuso.h"

static mem_fail_cb _g_memfail = NULL;

/* Memory tracking is serious business! */
#if KUSO_MEM_TRACKING
#undef mem_alloc
#undef mem_alloc_cnt
#undef mem_zalloc
#undef mem_zalloc_cnt
#undef mem_realloc
#undef mem_realloc_cnt
#undef mem_free
#undef mem_strdup

#define mem_alloc       _mem_alloc
#define mem_alloc_cnt   _mem_alloc_cnt
#define mem_zalloc      _mem_zalloc
#define mem_zalloc_cnt  _mem_zalloc_cnt
#define mem_realloc     _mem_realloc
#define mem_realloc_cnt _mem_realloc_cnt
#define mem_free        _mem_free
#define mem_strdup      _mem_strdup

void* mem_alloc(size_t size);
void* mem_alloc_cnt(size_t num, size_t size);
void* mem_zalloc(size_t size);
void* mem_zalloc_cnt(size_t num, size_t size);
void* mem_realloc(void* ptr, size_t size);
void* mem_realloc_cnt(void* ptr, size_t num, size_t size);
void mem_free(void* ptr);
char* mem_strdup(const char* str);

typedef struct mem_entry_t mem_entry_t;
struct mem_entry_t {
  mem_entry_t* next;
  mem_entry_t* prev;

  int    type;
  int    freed;
  void*  ptr;
  size_t size;
  size_t cnt;
  int    line;
  char*  file;
};

static mem_entry_t* _g_meminfo = NULL;

static void* _memtrk_alloc_core(size_t size, size_t cnt, int zero, int line, char* file)
{
  void* ptr;
  mem_entry_t* ent;

  ptr = mem_alloc(size * cnt);
  if(ptr == NULL)
    return NULL;

  if(zero) {
    memset(ptr, 0, size * cnt);
  }

  ent = mem_alloc(sizeof(mem_entry_t));

  ent->type  = 0;
  ent->ptr   = ptr;
  ent->size  = size;
  ent->cnt   = cnt;
  ent->line  = line;
  ent->file  = strdup(file);
  ent->next  = _g_meminfo;
  ent->prev  = NULL;
  ent->freed = FALSE;

  if(_g_meminfo != NULL)
    _g_meminfo->prev = ent;

  _g_meminfo = ent;

  return ptr;
}

void* _memtrk_alloc(size_t size, int line, char* file)
{
  return _memtrk_alloc_core(size, 1, FALSE, line, file);
}

void* _memtrk_zalloc(size_t size, int line, char* file)
{
  return _memtrk_alloc_core(size, 1, TRUE, line, file);
}

void* _memtrk_alloc_cnt(size_t size, size_t cnt, int line, char* file)
{
  return _memtrk_alloc_core(size, cnt, FALSE, line, file);
}

void* _memtrk_zalloc_cnt(size_t size, size_t cnt, int line, char* file)
{
  return _memtrk_alloc_core(size, cnt, TRUE, line, file);
}

void* _memtrk_realloc(void* ptr, size_t size, int line, char* file)
{
  return _memtrk_realloc_cnt(ptr, size, 1, line, file);
}

void* _memtrk_realloc_cnt(void* ptr, size_t size, size_t cnt, int line, char* file)
{
  mem_entry_t* i;

  for(i = _g_meminfo; i != NULL; i = i->next) {
    if(i->ptr == ptr)
      break;
  }
  ASSERT(i != NULL, "Pointer not found in memory list");

  ptr = mem_realloc(ptr, size * cnt);

  i->ptr  = ptr;
  i->size = size;
  i->cnt  = cnt;
  (void)line;
  (void)file;

  return ptr;
}

char* _memtrk_strdup(const char* str, int line, char* file)
{
  char* ptr;
  ASSERT(str != NULL, "String is NULL");
  ptr = _memtrk_alloc(strlen(str)+1, line, file);
  memcpy(ptr, str, strlen(str)+1);
  return ptr;
}

void _memtrk_free(void* ptr, int line, char* file)
{
  mem_entry_t* i;
  (void)line;
  (void)file;

  for(i = _g_meminfo; i != NULL; i = i->next) {
    if(i->ptr == ptr)
      break;
  }
  ASSERT(i != NULL, "Pointer not found in memory list");
  ASSERT(i->freed != TRUE, "Pointer already freed");
  if(i->prev != NULL) i->prev->next = i->next;
  if(i->next != NULL) i->next->prev = i->prev;
  if(i == _g_meminfo) _g_meminfo = i->next;
  free(i->file);
  mem_free(i);

  mem_free(ptr);
}

int _memtrk_leaked(void)
{
  int cnt;
  mem_entry_t* i;

  for(cnt = 0, i = _g_meminfo; i != NULL; i = i->next) {
    if(i->freed) continue;
    cnt++;
  }

  return cnt;
}

void _memtrk_print_leaks(void)
{
  int cnt;
  mem_entry_t* i;
  mem_entry_t* l;

  for(cnt = 0, l = NULL, i = _g_meminfo; i != NULL; l = i, i = i->next);

  fprintf(stderr, "Memory allocations currently alive: %d\n", _memtrk_leaked());
  for(cnt = 0, i = l; i != NULL; l = i, i = i->prev) {
    if(i->freed) continue;
    cnt++;
    switch(i->type) {
      case 0: /* Memory */
        fprintf(stderr, "Allocation %d (Memory):\n", cnt);
        fprintf(stderr, "    Pointer : %p\n", i->ptr);
        fprintf(stderr, "    Size    : 0x%lX * 0x%lX (0x%lX)\n", i->size, i->cnt, i->size*i->cnt);
        fprintf(stderr, "    Created : Line %d in %s\n", i->line, i->file);
        break;
      default:
        fprintf(stderr, "Allocation %d (%d):\n", cnt, i->type);
        fprintf(stderr, "    Pointer : %p\n", i->ptr);
        fprintf(stderr, "    Size    : 0x%lX * 0x%lX (0x%lX)\n", i->size, i->cnt, i->size*i->cnt);
        fprintf(stderr, "    Created : Line %d in %s\n", i->line, i->file);
        break;
    }
  }
  fprintf(stderr, "\n");
}

#endif /* KUSO_MEM_TRACKING */

/* Real memory code is here! */

void mem_init(void)
{
#if KUSO_MEM_TRACKING
  _g_meminfo = NULL;
#endif
  _g_memfail = NULL;
}

void mem_finish(void)
{
#if KUSO_MEM_TRACKING
  if(_memtrk_leaked() != 0) {
    _memtrk_print_leaks();
  }
#endif
}

static void _mem_fail_callback(void* old, size_t num, size_t size)
{
  if(_g_memfail != NULL) {
    _g_memfail(old, num, size);
  }
}

void mem_set_callback(mem_fail_cb cb)
{
  _g_memfail = cb;
}

/* Base memory stuff */
void* mem_alloc(size_t size)
{
  void* ptr;
  ptr = malloc(size);
  if(ptr == NULL) {
    _mem_fail_callback(NULL, 0, size);
  }
  return ptr;
}

void* mem_alloc_cnt(size_t num, size_t size)
{
  void* ptr;
  ptr = malloc(size * num);
  if(ptr == NULL) {
    _mem_fail_callback(NULL, num, size);
  }
  return ptr;
}

void* mem_zalloc(size_t size)
{
  void* ptr;
  ptr = malloc(size);
  if(ptr == NULL) {
    _mem_fail_callback(NULL, 0, size);
  }else{
    memset(ptr, 0, size);
  }
  return ptr;
}

void* mem_zalloc_cnt(size_t num, size_t size)
{
  void* ptr;
  ptr = malloc(size * num);
  if(ptr == NULL) {
    _mem_fail_callback(NULL, num, size);
  }else{
    memset(ptr, 0, size * num);
  }
  return ptr;
}

void* mem_realloc(void* ptr, size_t size)
{
  void *optr = ptr;
  ptr = realloc(ptr, size);
  if(ptr == NULL) {
    _mem_fail_callback(optr, 0, size);
  }
  return ptr;
}

void* mem_realloc_cnt(void* ptr, size_t num, size_t size)
{
  void *optr = ptr;
  ptr = realloc(ptr, size * num);
  if(ptr == NULL) {
    _mem_fail_callback(optr, num, size);
  }
  return ptr;
}

char* mem_strdup(const char* str)
{
  char* ptr;
  int len;

  if(str == NULL)
    return NULL;

  len = strlen(str)+1;
  ptr = mem_alloc(len);
  if(ptr == NULL)
    return NULL;
  memcpy(ptr, str, len);
  return ptr;
}

void mem_free(void* ptr)
{
  if(ptr == NULL) {
    _mem_fail_callback(ptr, 0, 0);
  }else{
    free(ptr);
  }
}
