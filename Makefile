OBJECTS	?=
OBJECTS	+= \
	src/main.o

OBJECTS	+= \
	src/kuso/kuso_list.o \
	src/kuso/kuso_mem.o \
	src/kuso/kuso_str.o \
	src/kuso/kuso_tkn.o

OUTPUT	?= nc86opt
CFLAGS	?= -Isrc -std=c11 -Wall -Wextra -pedantic
LDFLAGS	?= 
CC	?= gcc
RM	?= rm
INSTALL_PATH	?= /usr/local

.PHONY: all clean install uninstall

all: $(OBJECTS) $(OUTPUT)
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $+
clean:
	$(RM) -f $(OUTPUT) $(OBJECTS)
install:
	install	$(OUTPUT) $(INSTALL_PATH)/bin
uninstall:
	install	/dev/null $(INSTALL_PATH)/bin/$(OUTPUT)
